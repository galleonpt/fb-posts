<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;

use Symfony\Component\HttpClient\HttpClient;
use App\Models\User;
use App\Models\Page;
use App\Models\Post;
use Exception;

class SendPosts extends Job implements ShouldQueue
{
  use InteractsWithQueue, Queueable, SerializesModels;

  private $post;
  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct($post)
  {
    $this->post = $post;
  }

  /**
   * Execute the job.
   *
   *  Job que vai ser executado no Redis
   * 
   * @return void
   */
  public function handle()
  {
    // funçao para fazer os posts no fb
    $client = HttpClient::create();

    $url = "https://graph.facebook.com/" . $this->post->FbPageId . "/feed";

    $post = $client->request(
      'POST',
      $url,
      [
        'body' => [
          'message' => $this->post->message,
          'access_token' => $this->post->PageAccessToken
        ]
      ]
    );

    echo ("Post successfully sent!" . json_encode($this->post));
  }
}

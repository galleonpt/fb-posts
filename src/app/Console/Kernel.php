<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use App\Jobs\SendPosts;
use Illuminate\Support\Facades\DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            //All posts from each minutes
            // $posts = Post::where('send_time', '=', date('Y-m-d H:i'))->get();

            $posts = DB::table('Posts')
                ->join('Pages', 'Pages.id', '=', 'Posts.PageID')
                ->where('send_time', '=', date('Y-m-d H:i'))
                ->select(array(
                    'Pages.FbAccessToken as PageAccessToken',
                    'Posts.message as message',
                    'Pages.FbID as FbPageId',
                ))
                ->get();

            // dispatch(new SendPosts($posts));
            foreach ($posts as $post) {
                dispatch(new SendPosts($post));
            }
        })
            ->description('Enviou o Post')
            ->everyMinute();
    }
}

<?php

namespace App\Http\Controllers;

require_once $_SERVER['DOCUMENT_ROOT'] . '/../vendor/autoload.php';

use Symfony\Component\HttpClient\HttpClient;
use App\Models\User;
use App\Models\Page;
use App\Models\Post;
use Exception;
use Illuminate\Http\Request;

use Laravel\Lumen\Routing\Controller as BaseController;

class PagesController extends BaseController
{
  /**
   * Function to get all facebook pagres from one user
   *
   * @param int $id
   * @return void
   */
  public function GetAllPagesFromUser($id)
  {
    try {
      $user = User::find($id);

      if (!$user) {
        return response('User not found!', 400);
      }

      // $FBUserID = $user->FbUserID;
      $accessToken = $user->FbAccessToken;
      $FBUserID = '100653288749638';
      $accessToken = 'EAAGb0LssA0gBAEoufxpZCWYoUL3KakJA8NQ8cSaOfxXiuHZAYRacy5DAl8r7AMeZBX5udPbMv9uJsZCQuZBrYtXPZAkjnbZAONRBtHDTYxJ6YhPL7AOfYmeL9axLrNJbXIaDb4ZCAgiHd2ZBnhopQJynd3LlE2WpBKlPUPorAdbGFSONEz6bzoX1j';

      $client = HttpClient::create();

      $url = "https://graph.facebook.com/v10.0/$FBUserID/accounts"; //TODO passar o url para o .env

      $pages = $client->request('GET', $url, [
        'query' => [
          'access_token' => "$accessToken",
        ]
      ]);

      $pagesArray = json_decode($pages->getContent());

      foreach ($pagesArray->data as $data) {
        $pageInfo = [
          'FbID' => $data->id,
          'name' => $data->name,
          'FbAccessToken' => $data->access_token,
          'userID' => $user->id
        ];

        $alreadyInserted = Page::where('FbID', $pageInfo['FbID'])->where('userID', $pageInfo['userID'])->first();

        if (isset($alreadyInserted)) {
          return response('Page already inserted!', 422);
        }

        $data = Page::create($pageInfo);
        $data->save();
      }
      return response('Pages Inserted successfully', 201);
    } catch (Exception $e) {
      return response($e->getMessage(), 500);
    };
  }
}

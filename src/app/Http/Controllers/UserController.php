<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Validations\UserValidation;
use Exception;
use App\Models\User;

class UserController extends Controller
{
    use UserValidation;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function create(Request $request)
    {
        try {
            $this->CreateAndUpdate($request);

            $userdata = $request->all();

            $alreadyexists = User::where('username', $userdata['username'])->first();

            if ($alreadyexists) return response('User already exists!', 422);

            $hashedpw = password_hash($userdata['password'], PASSWORD_BCRYPT, ["cost" => 10]);

            $user = User::create([
                'username' => $userdata['username'],
                'password' => $hashedpw,
            ]);

            $user->save();

            return response('User criado com sucesso', 201);
        } catch (Exception $e) {
            return response($e->getMessage(), 500);
        }
    }

    public function show()
    {
        try {
            $users = User::all();

            return response($users, 200);
        } catch (Exception $e) {
            return response($e->getMessage(), 500);
        }
    }

    public function destroy($id)
    {
        try {
            User::where('id', $id)->delete();

            return response('', 204);
        } catch (Exception $e) {
            return response($e->getMessage(), 500);
        }
    }

    public function update($id, Request $request)
    {
        try {
            $user = User::where('id', $id)->first();

            if (!isset($user)) {
                return response("User not found!", 404);
            }

            $this->CreateAndUpdate($request);

            if (isset($request->all()['username'])) {
                $user->update(['username' => $request->all()['username']]);
            }

            if (isset($request->all()['password'])) {
                $user->update(['password' => password_hash($request->all()['password'], PASSWORD_DEFAULT)]);
            }

            return response("User updated sucessfully!", 200);
        } catch (Exception $e) {
            return response($e->getMessage(), 500);
        }
    }
}

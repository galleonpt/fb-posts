<?php

namespace App\Http\Controllers;

require_once $_SERVER['DOCUMENT_ROOT'] . '/../vendor/autoload.php';

use App\Models\Page;
use App\Models\Post;
use Exception;
use Illuminate\Http\Request;
use \App\Http\Controllers\Validations\PostValidation;
use Laravel\Lumen\Routing\Controller as BaseController;

class PostsController extends BaseController
{
  use PostValidation;

  public function create(Request $request)
  {
    try {

      $this->CreateValidation($request);
      echo (123);
      // $internalID = $request->only('internalPageId');
      // $message = ($request->only('message'))['message'];

      // $page = Page::find($internalID['internalPageId']);

      // $PageAccessToken = $page->FbAccessToken;

      // $postData = [
      //   "PageID" => $page->id,
      //   "PageAccessToken" => $PageAccessToken,
      //   "send_time" => ($request->only('sendTime'))['sendTime'],
      //   'message' => $message
      // ];

      // $postData = Post::create($postData);
      // $postData->save();

      // return response('Post schedulled successfully!', 201);
    } catch (Exception $e) {
      return response($e->getMessage(), 500);
    }
  }


  public function destroy($id)
  {
    try {
      Post::find($id)->delete();
      return response('', 204);
    } catch (Exception $e) {
      return response($e->getMessage(), 500);
    }
  }

  public function update($id, Request $request)
  {
    try {

      $this->UpdateValidation($request);


      $post = Post::find($id);

      if (!isset($post)) {
        return response("Post not found!", 404);
      }



      if (isset($request->all()['send_time'])) {
        $post->update(['send_time' => $request->all()['send_time']]);
      }

      if (isset($request->all()['message'])) {
        $post->update(['message' => $request->all()['message']]);
      }

      return response("Post updated sucessfully!", 200);
    } catch (Exception $e) {
      return response($e->getMessage(), 500);
    }
  }
}

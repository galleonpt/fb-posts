<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Laravel\Lumen\Routing\Controller as BaseController;
use Exception;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Validations\LoginRegisterValidation;

class AuthController extends BaseController
{
  use LoginRegisterValidation;

  public function Login(Request $request)
  {
    //validate inputs on request
    $this->LoginAndRegister($request);

    $credentials = $request->only(['username', 'password']);

    $alreadyexists = User::where('username', $credentials['username'])->first();

    if (!$alreadyexists) return response('User doesn\'t exists!', 422);

    if (!$token = Auth::attempt($credentials)) { // Generate token if exists this user
      return response()->json(['message' => 'Unauthorized'], 401); // Unauthorized
    }
    return $this->respondWithToken($token); // Create token response
  }


  // Register
  public function Register(Request $request)
  {
    //validate inputs on request
    $this->LoginAndRegister($request);

    $body = $request->all();

    if (isset($body['username']) && isset($body['password'])) {

      $alreadyexists = User::where('username', $body['username'])->first();

      if ($alreadyexists) return response('User already exists!', 422);

      $register = array( // Set user object
        'username' => $request->input('username'),
        'password' => password_hash($request->input('password'), PASSWORD_DEFAULT),
      );
      try {
        $user = User::Create($register); // Create user
        return response($user, 201); // Created
      } catch (Exception $e) {
        return response($e->getMessage(), 500); // Internal Server Error
      }
    } else {
      return response('Bad Request', 400); // Bad Request
    }
  }

  /**
   * Function to return an object with JWT token information
   *
   * @param string $token
   * @return void
   */
  public function respondWithToken($token)
  {
    return response()->json([
      'token' => $token,
      'token_type' => 'bearer',
      'expires_in' => Auth::factory()->getTTL() * 60
    ], 200);
  }
}

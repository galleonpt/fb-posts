<?php

namespace App\Http\Controllers\Validations;

use Illuminate\Http\Request;

trait PostValidation
{
  public function CreateValidation(Request $request)
  {
    return $this->validate($request, [
      'internalPageId' => 'integer|required',
      'message' => 'string|required',
      'sendTime' => 'date|required',
    ]);
  }

  public function UpdateValidation(Request $request) //TODO o erro que retorna está hard coded(personalizar o erro)
  {
    return $this->validate($request, [
      'send_time' => 'date|required',
      'message' => 'string|required',
    ]);
  }
}

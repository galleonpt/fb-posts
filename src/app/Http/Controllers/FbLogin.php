<?php


namespace App\Http\Controllers;

require_once $_SERVER['DOCUMENT_ROOT'] . '/../vendor/autoload.php';

session_start();

use Laravel\Lumen\Routing\Controller as BaseController;
use Facebook\Facebook as Facebook;
use App\Models\User;

class FbLogin extends BaseController
{
  public function FbLogin()
  {
    $fb = new Facebook([
      'app_id' => getenv('FB_APPID'),
      'app_secret' => getenv('FB_APPSECRET'),
      'default_graph_version' => getenv('FB_DEFAULT_GRAPH_VERSION'),
    ]);

    $helper = $fb->getRedirectLoginHelper();

    $permissions = ['pages_manage_posts'];
    $loginUrl = $helper->getLoginUrl('http://localhost/fb-callback', $permissions); //TODO enviar o url para o client e ir buscar os dados nos params


    return redirect()->to($loginUrl);
  }

  public function FbCallback()
  {
    $fb = new Facebook([
      'app_id' => getenv('FB_APPID'),
      'app_secret' => getenv('FB_APPSECRET'),
      'default_graph_version' => getenv('FB_DEFAULT_GRAPH_VERSION'),
    ]);

    $helper = $fb->getRedirectLoginHelper();
    try {
      $accessToken = $helper->getAccessToken();
    } catch (\Facebook\Exceptions\FacebookResponseException $e) {
      // When Graph returns an error
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch (\Facebook\Exceptions\FacebookSDKException $e) {

      response('Facebook SDK returned an error: ' . $e->getMessage(), 400);
      exit;
    }

    if (!isset($accessToken)) {
      if ($helper->getError()) {
        header('HTTP/1.0 401 Unauthorized');
      } else {
        header('HTTP/1.0 400 Bad Request');
      }
      exit;
    }
    // The OAuth 2.0 client handler helps us manage access tokens
    $oAuth2Client = $fb->getOAuth2Client();
    // Get the access token metadata from /debug_token
    $tokenMetadata = $oAuth2Client->debugToken($accessToken);
    // Validation (these will throw FacebookSDKException's when they fail)
    $tokenMetadata->validateAppId(getenv('FB_APPID'));
    // If you know the user ID this access token belongs to, you can validate it here
    $tokenMetadata->validateExpiration();

    if (!$accessToken->isLongLived()) {
      try {
        $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
      } catch (\Facebook\Exceptions\FacebookSDKException $e) {
        response("Error getting long-lived access token: " . $e->getMessage(), 400);
        exit;
      }
    }

    $response = $fb->get('/me?fields=id', $accessToken);
    $userID = $response->getGraphUser()['id'];

    //id do user que esta no token - jwt nao esta a funcionar
    $userID_Authenticated = 1;

    $user = User::find($userID_Authenticated);

    $user->update([
      'FbUserID' => "$userID",
      'FbAccessToken' => "$accessToken"
    ]);
  }
}

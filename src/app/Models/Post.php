<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{


  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $table = 'Posts';
  public $timestamps = false;
  protected $fillable = [
    'PageID', 'PageAccessToken', 'send_time', 'message'
  ];
}

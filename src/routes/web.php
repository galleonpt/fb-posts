<?php

$router->get('/working', function () {
    return 'Server on 🔥 ';
});

//USERS
$router->get('/', "UserController@show");

//--  /authenticate
$router->group(['prefix' => 'authenticate'], function () use ($router) {
    $router->post('login', 'AuthController@Login');
    $router->post('register', 'AuthController@Register');
});

//--  /private
$router->group(['prefix' => 'private', 'middleware' => 'auth'], function () use ($router) {
    $router->put('/{id}', "UserController@update");
    $router->delete('/{id}', "UserController@destroy");

    $router->get('/fb', 'FbLogin@FbLogin');
    $router->get('/fb-callback', 'FbLogin@FbCallback');
    $router->get('/user-pages/{id}', 'PagesController@GetAllPagesFromUser');

    $router->post('/post-msg', 'PostsController@create');
    $router->delete('/posts/{id}', "PostsController@destroy");
    $router->put('posts/{id}', "PostsController@update");
});
